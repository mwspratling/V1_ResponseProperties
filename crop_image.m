function I=crop_image(I)
s=max(abs(I-0.5));
b_start=min(find(s>0));
b_end=max(find(s>0));
s=max(abs(I'-0.5));
a_start=min(find(s>0));
a_end=max(find(s>0));

I=I(a_start:a_end,b_start:b_end);