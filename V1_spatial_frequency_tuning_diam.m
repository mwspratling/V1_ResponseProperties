function V1_spatial_frequency_tuning_diam
crop=0;
spatial_freq=1./[20,12,8,6,4,3,2];
grating_wavels=1./spatial_freq;

iterations=12;
patch_diams=[7,13,19];
phase=0;
grating_angle=0;
contrast=0.5;
node=1;


[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
j=0;
for diam=patch_diams
  j=j+1;
  i=0;
  for gw=grating_wavels
	i=i+1;
	fprintf(1,'.%i.',i); 
	I=image_circular_grating(diam,20,gw,grating_angle,phase,contrast);
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	
	%plot original image
	maxsubplot(3,length(grating_wavels),i),
	imagesc(I(:,:,1),[0.25,0.75]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(grating_wavels),i+length(grating_wavels)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.025]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(grating_wavels),i+2*length(grating_wavels)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	sc(j,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

%for j=1:3, sc(j,:)=sc(j,:)./max(sc(j,:)); end

figure(1),clf
subplot(3,2,[4,6])
semilogx(spatial_freq,sc(3,:),'b-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
semilogx(spatial_freq,sc(2,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
semilogx(spatial_freq,sc(1,:),'g-d','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.05,0.5,0,0.022]);
set(gca,'YTick',[0:0.01:0.3],'XTick',[0.05,0.1,0.2,0.5],'FontSize',20,'Box','off');
xlabel('Spatial Freq. (cycles/pixel)'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
patch_diam=13;
maxdiam=patch_diam;
spacingh=25;
L=zeros(maxdiam+2,2*spacingh+maxdiam+2)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+1;
gwl=length(grating_wavels);
for gw=grating_wavels([1,ceil(gwl/2),gwl])
  I=image_circular_grating(patch_diam,0*patch_diam,gw,grating_angle,phase,contrast);   [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
axis([-7,67,0,maxdiam+2])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

