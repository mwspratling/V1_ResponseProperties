function I=image_uniform(size,len,gap,angle)
%draw a uniform texture
[line,len]=define_bar(len);
[I,size]=define_blank_image(size,len,gap);
angle=rem(angle,90);
[xlocations,ylocations]=define_grid(size,len,gap,angle);

for x=xlocations
  for y=ylocations
	I=draw_line(I,x,y,line,angle);
  end
end

I=crop_image(I);
