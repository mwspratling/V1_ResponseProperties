function V1_cross_orient_contrast
crop=0;
grating_wavel=6;
patch_diam=51;

iterations=20;
contrasts=[0,0.06,0.12,0.25,0.50];
phase=0;

mask_angle=90;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
i=0;
for contg=contrasts
  i=i+1;
  j=0;
  for contm=contrasts
	j=j+1;
	I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,...
								   0,mask_angle,phase,phase,contg,contm);
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	%plot original image
	maxsubplot(3,length(contrasts),i),
	imagesc(I,[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
  
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),i+length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,1]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition 
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),i+2*length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
		
	sc(i,j)=y(ceil(a/2),ceil(b/2),node);
  end
end

zeroOffset=0.012
blankx=0.008;
figure(1),clf
subplot(3,2,[4,6])
plot(log(zeroOffset+contrasts),sc(:,1),'r-o','LineWidth',1,'MarkerSize',6,'MarkerFaceColor','r');
hold on
plot(log(zeroOffset+contrasts),sc(:,2),'r-o','LineWidth',2,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(:,3),'r-o','LineWidth',3,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(:,4),'r-o','LineWidth',4,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(:,5),'r-o','LineWidth',5,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+blankx+[0,0]),[0,0.5],'w','LineWidth',22);
axis([log(zeroOffset),log(zeroOffset+1),0,0.011])
set(gca,'XTick',log(zeroOffset+[0.1,0.5,1]),'YTick',[0:0.002:0.011]);
set(gca,'XTickLabel',[0.1,0.5,1],'FontSize',20,'Box','off');
xlabel('Test Contrast'),ylabel('Response')

figure(2),clf
subplot(3,2,[4,6])
plot(log(zeroOffset+contrasts),sc(1,:),'r-o','LineWidth',1,'MarkerSize',6,'MarkerFaceColor','r');
hold on
plot(log(zeroOffset+contrasts),sc(2,:),'r-o','LineWidth',2,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(3,:),'r-o','LineWidth',3,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(4,:),'r-o','LineWidth',4,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(5,:),'r-o','LineWidth',5,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+blankx+[0,0]),[0,0.5],'w','LineWidth',22);
axis([log(zeroOffset),log(zeroOffset+1),0,0.011])
set(gca,'XTick',log(zeroOffset+[0.1,0.5,1]),'YTick',[0:0.002:0.011]);
set(gca,'XTickLabel',[0.1,0.5,1],'FontSize',20,'Box','off');
xlabel('Mask Contrast'),ylabel('Response')

figure(3),clf
maxsubplot(5,2,8)
spacingh=ceil((275-patch_diam)/2);
L=zeros(patch_diam,2*spacingh+patch_diam)+0.5;
centv=ceil(patch_diam/2)+1;
centh=ceil(patch_diam/2)+1;
for contg=contrasts([1,3,5])
  I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,0,mask_angle,0,0,contg,0.5);
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=50-patch_diam/2;
axis([-ends,2*spacingh+patch_diam+ends,-25,patch_diam])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

figure(4),clf
maxsubplot(5,2,8)
spacingh=ceil((275-patch_diam)/2);
L=zeros(patch_diam,2*spacingh+patch_diam)+0.5;
centv=ceil(patch_diam/2)+1;
centh=ceil(patch_diam/2)+1;
for contm=contrasts([1,3,5])
  I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,0,mask_angle,0,0,0.5,contm);
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=50-patch_diam/2;
axis([-ends,2*spacingh+patch_diam+ends,-25,patch_diam])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

