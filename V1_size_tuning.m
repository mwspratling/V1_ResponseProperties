function sc=V1_size_tuning
crop=0;
grating_wavel=6;

diams=[3:4:31];

iterations=8;
contrast=0.25;
phase=0;

node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
for test=1:2
  i=0;
  clear Ion Ioff;
  for diam=diams
	i=i+1;
	fprintf(1,'.%i.',i); 
	if test==1 %a grating
	  I=image_contextual_surround(diam,max(diams)-diam/2,0,grating_wavel,grating_wavel,0,phase,0,contrast,0);
	else %an annulus
	  I=image_contextual_surround(0,diam/2,max(diams)-diam/2,grating_wavel,grating_wavel,0,0,phase,0,contrast);
	end
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	
	%plot original image
	maxsubplot(3,length(diams),i),
	imagesc(I(crop+1:a-crop,crop+1:b-crop,:),[0,1]); 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(diams),i+length(diams)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(diams),i+2*length(diams)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	sc(test,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

figure(1),clf
subplot(3,2,[4,6])
plot(diams,sc(1,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','r');
hold on
plot(diams,sc(2,:),'b-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([1,33,0,0.008])
set(gca,'XTick',[5:5:30],'YTick',[0:0.002:0.008],'FontSize',20,'Box','off');
xlabel('Diameter (pixels)'),ylabel('Response')
figure(2),clf
maxsubplot(5,2,8)
maxdiam=max(diams).*2;
spacingh=maxdiam*2;(450-maxdiam)/2;
spacingv=max(40,maxdiam)
L=zeros(spacingv+maxdiam+1,2*spacingh+maxdiam+1)+0.5;
centv=ceil(maxdiam/2)+1;
for test=1:2
  centh=ceil(maxdiam/2)+1;
  for diam=diams([1,5,7])
	if test==1
	  I=image_contextual_surround(diam,max(diams)-diam/2,0,grating_wavel,grating_wavel,0,0,0,contrast,0);%a grating
	else
	  I=image_contextual_surround(0,diam/2,max(diams)-diam/2,grating_wavel,grating_wavel,0,0,0,0,contrast);%an annulus
	end
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
	centh=centh+spacingh;
  end
  centv=centv+spacingv;
end
imagesc(L);
axis('equal','off')
ends=maxdiam/2;
axis([-ends,2*spacingh+maxdiam+ends,max(diams)/2,2*maxdiam])
hold on
centv=ceil(maxdiam/2)+1;
plot(mean([maxdiam+15,spacingh-15]),centv,'ro','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','r');
plot(mean([maxdiam+spacingh+15,2*spacingh-15]),centv,'ro','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','r');
centv=centv+spacingv;
plot(mean([maxdiam+15,spacingh-15]),centv,'bo','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(mean([maxdiam+spacingh+15,2*spacingh-15]),centv,'bo','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

