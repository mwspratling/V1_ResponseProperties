function I=image_isolated(size,len,gap,angle)
%draw a single bar in a blank image
[line,len]=define_bar(len);
[I,size]=define_blank_image(size,len,gap);
angle=rem(angle,90);
[xlocations,ylocations]=define_grid(size,len,gap,angle);

a=0;
for x=xlocations
  a=a+1;
  b=0;
  for y=ylocations
	b=b+1;
	if a==ceil(length(xlocations)/2) & b==ceil(length(ylocations)/2)
	  I=draw_line(I,x,y,line,angle);
	else
	  I=draw_inverse_line(I,x,y,line,-angle);
	end
  end
end
I=crop_image(I);
I(find(I<0.5))=0.5;
