function [line,len]=define_bar(len,wid);
if nargin<2, wid=1; end
if mod(len,2)==0, len=len+1; end %give bars an odd length
if mod(wid,2)==0, wid=wid+1; end %give bars an odd width
hlen=fix(len/2);
hwid=fix(wid/2);
line=zeros(len+2);
line(hlen+2-hwid:hlen+2+hwid,2:len+1)=1;

