function V1_cross_orient_orient_2masks
clear
crop=0;
grating_wavel=6;
patch_diam=51;

iterations=12;
contrast=0.5;
phase=0;

grating_angles=[-90:22.5:90];
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
for test=1:2
  i=0;
  for angle=grating_angles
	i=i+1;
	fprintf(1,'.%i.',i); 
	if test==1 %cross-orientation simulus
	  I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,...
									 0,angle,phase,phase,contrast,contrast);
	elseif test==2 %cross-orientation simulus with 2 masks
	  I=image_cross_orientation_sine_2masks(patch_diam,grating_wavel,...
											grating_wavel,0,angle,phase,phase,...
											contrast,contrast/2);
	else %orientation tuning stimulus
	  I=image_square_grating(patch_diam,0,grating_wavel,angle, ...
							 phase,contrast*2); 
	end
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	%plot original image
	maxsubplot(3,length(grating_angles),i),
	imagesc(I(:,:),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(grating_angles),i+length(grating_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(grating_angles),i+2*length(grating_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;

	sc(test,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

figure(1),clf
subplot(3,2,[4,6])
plot(grating_angles,sc(1,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
plot(grating_angles,sc(2,:),'g-d','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([-90,90,0,0.014])
set(gca,'XTick',[-90:45:90],'YTick',[0:0.002:0.015],'FontSize',20,'Box','off');
xlabel('Orientation (degrees)'),ylabel('Response')

figure(2),clf
maxsubplot(3,2,6)
spacingh=patch_diam*2;
spacingv=patch_diam*1.5;
L=zeros(patch_diam+1*spacingv+1,2*spacingh+patch_diam+1)+0.5;
centv=ceil(patch_diam/2)+1;
gal=length(grating_angles);
for test=1:2
  centh=ceil(patch_diam/2)+1;
  for angle=grating_angles([2,ceil(gal/2),gal-1])
	phase=0;
	if test==1 %cross-orientation simulus
	  I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,...
									 0,angle,phase,phase,contrast,contrast);
	  elseif test==2 %cross-orientation simulus with 2 masks
		I=image_cross_orientation_sine_2masks(patch_diam,grating_wavel,grating_wavel,...
									   0,angle,phase,phase,contrast,contrast/2);
	else %orientation tuning stimulus
	  I=image_square_grating(patch_diam,0,grating_wavel,angle, ...
							 phase,contrast*2);
	end
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
	centh=centh+spacingh;
  end
  centv=centv+spacingv;
end
imagesc(L);
axis('equal','off')
ends=90;
axis([-ends,2*spacingh+patch_diam+ends,0,patch_diam+1*spacingv])
hold on
plot(mean([spacingh,patch_diam]),ceil(patch_diam/2)+1,'ro','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(mean([spacingh,patch_diam]),ceil(patch_diam/2)+1+spacingv,'gd','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');

plot(mean([spacingh+patch_diam,2*spacingh]),ceil(patch_diam/2)+1,'ro','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(mean([spacingh+patch_diam,2*spacingh]),ceil(patch_diam/2)+1+spacingv,'gd','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');

cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
