function V1_surround_suppression_orient
crop=0;
grating_wavel=6;
phase=0;
node=1;
levitt=0

if levitt 
  %for Levitt and Lund experiment
  contrast_hi=0.75; 
  contrast_lo=0.25; 
  tests=4; 
  context_angles=[-90:30:90];
  patch_diam=15;iterations=20;
else
  %for Jones and Sillito experiment
  contrast_hi=0.5; 
  tests=2; 
  context_angles=[-90:22.5:90];
  %patch_diam=7;iterations=12;%gives orientation contrast suppression
  %patch_diam=11;iterations=8;%gives non-orientation specific suppression
  %patch_diam=13;iterations=12;%gives mixed general suppression
  %patch_diam=17;iterations=16;%gives orientation alignment suppression
  %patch_diam=19;iterations=16;%gives orientation contrast facilitation
  patch_diam=15;iterations=20;
end
maxdiam=24;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
for test=1:tests
  i=0;
  clear Ion Ioff;
  for ca=context_angles
	i=i+1;
	fprintf(1,'.%i.',i); 
	if test==1 %contextual surround
	  Ig=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,...
								   grating_wavel,ca,phase,phase,...
								   contrast_hi,contrast_hi); 
	elseif test==2 %orientation tuning
	  Ig=image_circular_grating(patch_diam,patch_diam,grating_wavel,ca,phase,...
								contrast_hi); 
	elseif test==3 %contextual surround with low contrast centre
	  Ig=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel, ...
								   grating_wavel,ca,phase,phase,contrast_lo, ...
								   contrast_hi); 
	else %orientation tuning with low contrast centre
	  Ig=image_circular_grating(patch_diam,patch_diam,grating_wavel,ca,phase,...
								contrast_lo); 
	end
	%pad image so that they are the same size regardless of patch diameter
	I=zeros(3*maxdiam)+0.5;
	[a,b]=size(I);
	[ag,bg]=size(Ig);
	I(ceil(a/2)-floor(ag/2):ceil(a/2)+floor(ag/2),ceil(b/2)-floor(bg/2):ceil(b/2)+floor(bg/2))=Ig;
	[Ion,Ioff]=preprocess_image(I);
	%plot original image
	maxsubplot(3,length(context_angles),i),
	imagesc(I(:,:,1),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(context_angles),i+length(context_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(context_angles),i+2*length(context_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	sc(test,i)=y(ceil(a/2),ceil(b/2),node);
  end
end
figure(1),clf
subplot(3,2,[4,6])
plot(context_angles,sc(1,:),'r-',context_angles,sc(2,:),'b--','LineWidth',6);
if levitt
  hold on
  plot(context_angles,sc(3,:),'g-.','LineWidth',6)
  plot([-90,90],sc(4,4).*[1,1],'k-','LineWidth',3)
end
if levitt
  axis([-90,90,0,0.03]) 
else
  axis([-90,90,0,0.024])
end
set(gca,'XTick',[-90:45:90],'YTick',[0:0.01:0.03],'FontSize',20,'Box','off');
xlabel('Orientation (degrees)'),ylabel('Response')
figure(2),clf
maxdiam=3*maxdiam;
if levitt
  maxsubplot(3,2,6)
  spacingh=(420-maxdiam)/2;
  spacingv=max(20,maxdiam-patch_diam/2);
else
  maxsubplot(5,2,8)
  spacingh=(400-maxdiam)/2;
  spacingv=max(20,maxdiam);
end
if levitt
  L=zeros(3*spacingv+maxdiam,2*spacingh+maxdiam+2)+0.5;
else
  L=zeros(spacingv+maxdiam-patch_diam+3,2*spacingh+maxdiam+2)+0.5;
end
centv=ceil(maxdiam/2)+1;
for test=1:tests
  centh=ceil(maxdiam/2)+1;
  for ca=[-90,0,90]
	if test==1 %contextual surround
	  I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,grating_wavel,ca,phase,phase,contrast_hi,contrast_hi); 
	elseif test==2 %orientation tuning
	  I=image_circular_grating(patch_diam,patch_diam,grating_wavel,ca,phase,contrast_hi); 
	elseif test==3 %contextual surround with low contrast centre
	  I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel, ...
								   grating_wavel,ca,phase,phase,contrast_lo, ...
								   contrast_hi); 
	else %orientation tuning with low contrast centre
	  I=image_circular_grating(patch_diam,patch_diam,grating_wavel,ca,phase, ...
								contrast_lo); 
	end
	[a,b]=size(I);
	if test~=4 | ca==0
	  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
	end
	centh=centh+spacingh;
  end
  centv=centv+spacingv;
end
imagesc(L);
axis('equal','off')
ends=0;
if levitt
  axis([-ends,2*spacingh+maxdiam+ends,0,4*spacingv])
else
  axis([-ends,2*spacingh+maxdiam+ends,0,2*spacingv-maxdiam])
end
hold on
centv=ceil(maxdiam/2)+1;
plot((maxdiam+spacingh)/2+[-20,20],[centv,centv],'r-','LineWidth',6,'MarkerSize',12,'MarkerFaceColor','w');
plot((maxdiam+3*spacingh)/2+[-20,20],[centv,centv],'r-','LineWidth',6,'MarkerSize',12,'MarkerFaceColor','w');
centv=centv++spacingv;
plot((maxdiam+spacingh)/2+[-20,20],[centv,centv],'b--','LineWidth',6,'MarkerSize',12,'MarkerFaceColor','w');
plot((maxdiam+3*spacingh)/2+[-20,20],[centv,centv],'b--','LineWidth',6,'MarkerSize',12,'MarkerFaceColor','w');

if levitt
  centv=centv++spacingv;
  plot((maxdiam+spacingh)/2+[-20,20],[centv,centv],'g-.','LineWidth',6);
  plot((maxdiam+3*spacingh)/2+[-20,20],[centv,centv],'g-.','LineWidth',6);
  centv=centv++spacingv;
  plot((maxdiam+spacingh)/2+[-20,20],[centv,centv],'k-','LineWidth',3);
  plot((maxdiam+3*spacingh)/2+[-20,20],[centv,centv],'k-','LineWidth',3);
end

cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

