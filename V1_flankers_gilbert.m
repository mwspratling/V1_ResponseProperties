function V1_flankers_gilbert
crop=0;
iterations=8;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;
node=1;

%DEFINE TEST IMAGES
Im=image_gilbert;

%find location of centre of RF
xcoord=mean(find(max(Im{1})>0.75))
ycoord=mean(find(max(Im{1}')>0.75))


clf
for k=1:length(Im)
  fprintf(1,'.%i.',k); 
  I=Im{k};
  [Ion,Ioff]=preprocess_image(I);
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(Im),k),
  imagesc(I(crop+1:a-crop,crop+1:b-crop,:),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(Im),k+length(Im)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;

  %perform competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(Im),k+2*length(Im)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;

  resp(k)=y(xcoord,ycoord,node);
end

cmap=colormap('gray');

figure(1),clf
axes('Position',[0.145,0.55,0.79,0.4]),bar([1:length(Im)],resp,'b')
top=max(max(resp));
axis([0.25,9.75,0,1.05*top])
set(gca,'YTick',[0:0.001:0.005],'XTickLabel',[],'FontSize',20,'Box','off');
ylabel('Response')
crop=0;
for i=1:length(Im)
  maxsubplot(7,12,38+i);imagesc(Im{i}(crop+1:a-crop,crop+1:b-crop),[0,1])
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
end