function V1_surround_suppression_plaid_contrast
crop=0;
grating_wavel=6;
contrasts=[0,0.01,0.025,0.05,0.1,0.25,0.5,0.8,1.25];
iso_contrast=0.3;
node=1;
patch_diam=13;
iterations=30;
phase=0;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
i=0;
for orth_contrast=contrasts
  i=i+1;
  fprintf(1,'.%i.',i); 
  %contextual surround:
  I=image_contextual_surround_plaid(patch_diam,0,20,grating_wavel,...
									grating_wavel,grating_wavel,0,90,...
									phase,phase,phase,...
									iso_contrast,iso_contrast,orth_contrast);
  if orth_contrast>1 %centre patch alone
	I=image_contextual_surround(patch_diam,20,0,grating_wavel,grating_wavel,...
								0,0,0,iso_contrast,0);
  end
  [Ion,Ioff]=preprocess_image(I);
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(contrasts),i),
  imagesc(I(:,:,1),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(contrasts),i+length(contrasts)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %perform competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(contrasts),i+2*length(contrasts)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
    
  sc(i)=y(ceil(a/2),ceil(b/2),node);
end
figure(1),clf
zeroOffset=0.002;
blankx=0.002;
subplot(3,2,[4,6])
plot(log(zeroOffset+contrasts(1:length(sc)-1)),sc(1:length(sc)-1),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([log(zeroOffset),log(zeroOffset+1),0,0.032])
set(gca,'XTick',log(zeroOffset+[0,0.01,0.1,1]),'YTick',[0:0.01:0.06]);
set(gca,'XTickLabel',[0,0.01,0.1,1],'FontSize',20,'Box','off');
xlabel('Contrast'),ylabel('Response')
hold on
%plot activity to centre alone
plot(log(zeroOffset+blankx+[0,0]),[0,0.5],'w','LineWidth',15);
plot(log(zeroOffset+[0,1]),sc(length(sc)).*[1,1],'b-','LineWidth',4);

figure(2),clf
maxsubplot(5,2,8)
maxdiam=3*patch_diam;
spacingh=(275-maxdiam)/2;
L=zeros(maxdiam,2*spacingh+maxdiam)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+1;
for orth_contrast=[0,0.16,0.8]
  I=image_contextual_surround_plaid(patch_diam,0,patch_diam,grating_wavel,grating_wavel,grating_wavel,0,90,0,0,0,iso_contrast,iso_contrast,orth_contrast);%contextual surround
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=60-maxdiam/2;
axis([-ends,2*spacingh+maxdiam+ends,0,maxdiam])
hold on
centv=ceil(maxdiam/2)+1;
plot(mean([maxdiam,spacingh]),centv,'ro','LineWidth',4,'MarkerSize',8,'MarkerFaceColor','w');
plot(mean([maxdiam+spacingh,2*spacingh]),centv,'ro','LineWidth',4,'MarkerSize',8,'MarkerFaceColor','w');
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
