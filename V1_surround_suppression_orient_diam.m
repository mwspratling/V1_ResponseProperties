function V1_surround_suppression_orient_diam
clear
crop=0;
grating_wavel=6;
context_angles=[-90:22.5:90];
patch_diams=[7,13,19];
iterations=12;
contrast=0.5;
phase=0;
node=1;
maxdiam=20;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
j=0;
for diam=patch_diams
  j=j+1;
  i=0;
  clear Ion Ioff;
  for ca=context_angles
	i=i+1;
	fprintf(1,'.%i.',i); 
	Ig=image_contextual_surround(diam,0,20,grating_wavel,grating_wavel,ca,phase,phase,contrast,contrast);
	%pad image so that they are the same size regardless of patch diameter
	I=zeros(3*maxdiam)+0.5;
	[a,b]=size(I);
	[ag,bg]=size(Ig);
	I(ceil(a/2)-floor(ag/2):ceil(a/2)+floor(ag/2),ceil(b/2)-floor(bg/2):ceil(b/2)+floor(bg/2))=Ig;
	[Ion,Ioff]=preprocess_image(I);
	%plot original image
	maxsubplot(3,length(context_angles),i),
	imagesc(I(:,:,1),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(context_angles),i+length(context_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.1]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(context_angles),i+2*length(context_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.025]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	sc(j,i)=y(ceil(a/2),ceil(b/2),node);
  end
end
figure(1),clf
subplot(3,2,[4,6])
plot(context_angles,sc(1,:),'g-d','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
plot(context_angles,sc(2,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(context_angles,sc(3,:),'b-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([-90,90,0,0.024])
set(gca,'XTick',[-90:45:90],'YTick',[0:0.01:0.03],'FontSize',20,'Box','off');
xlabel('Orientation (degrees)'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
patch_diam=13;
maxdiam=patch_diam;
spacingh=75;
L=zeros(maxdiam+42,2*spacingh+maxdiam+42)+0.5;
centv=ceil(maxdiam/2)+21;
centh=ceil(maxdiam/2)+21;
for ca=[-90,0,90]
  I=image_contextual_surround(patch_diam,0,20,grating_wavel,grating_wavel,ca,phase,phase,contrast,contrast); %contextual surround

  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
axis([-15,220,0,maxdiam+42])

cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

