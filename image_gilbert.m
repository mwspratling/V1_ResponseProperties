function Im=image_gilbert
size=27;
cent=ceil(size/2);
hlen=2;%3;
vgap=2;%3;
hgap=2;%3;
hwidth=0;

I=zeros(size)+0.5;

%central bar
Im{1}=I;
Im{1}(cent-hwidth:cent+hwidth,cent-hlen:cent+hlen)=1;

%colinear flankers
Im{3}=I;
Im{3}(cent-hwidth:cent+hwidth,cent-3*hlen-hgap:cent-hlen-hgap)=1;
Im{3}(cent-hwidth:cent+hwidth,cent+hlen+hgap:cent+3*hlen+hgap)=1;

%parallel flankers
Im{5}=I;
Im{5}(cent-vgap-hwidth:cent-vgap+hwidth,cent-hlen:cent+hlen)=1;
Im{5}(cent+vgap-hwidth:cent+vgap+hwidth,cent-hlen:cent+hlen)=1;

%centre + colinear
Im{2}=max(Im{1},Im{3});

%centre + parallel
Im{4}=max(Im{1},Im{5});


%from Das and Gilbert

%one perpendicular flanker
Im{8}=I;
Im{8}(cent-hlen:cent+hlen,cent-hlen-hgap)=1;

%one colinear flanker
Im{9}=I;
Im{9}(cent-hwidth:cent+hwidth,cent-3*hlen-hgap:cent-hlen-hgap)=1;

%22.5 degree flanker
%Im{9}(cent-2,6:7)=1;
%Im{9}(cent-1,8)=1;
%Im{9}(cent,9:10)=1;

%45 degree flanker
%Im{9}(cent,10)=1;%Im{9}(cent-1,9)=1;%Im{9}(cent-2,8)=1;%Im{9}(cent-3,7)=1;%Im{9}(cent-4,6)=1;

Im{6}=max(Im{1},Im{9});
Im{7}=max(Im{6},Im{8}); 
