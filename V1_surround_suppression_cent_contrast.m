function V1_surround_suppression_cent_contrast
crop=0;
iterations=20;
grating_wavel=6;

contrasts=[0.03,0.06,0.13,0.25,0.5];
patch_diam=15;

node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
for test=1:3
  i=0;
  clear Ion Ioff;
  for cont=contrasts
	i=i+1;
	fprintf(1,'.%i.',i); 
	if test==1 %no surround
	  I=image_contextual_surround(patch_diam,patch_diam,0,grating_wavel,...
								  grating_wavel,0,0,0,cont,0); 
	elseif test==2 %iso-oriented surround
	  I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,...
								  grating_wavel,0,0,0,cont,max(contrasts)); 
	else %perpendicular surround		
	  I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,...
								  grating_wavel,90,0,0,cont,max(contrasts)); 
	end
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	%plot original image
	maxsubplot(3,length(contrasts),i),
	imagesc(I(:,:,1),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),i+length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),i+2*length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
		
	sc(test,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

figure(1),clf
subplot(3,2,[4,6])
semilogx(contrasts,sc(1,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','r');
hold on
semilogx(contrasts,sc(2,:),'b-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
semilogx(contrasts,sc(3,:),'g-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.01,1,0,0.03])
set(gca,'XTick',[0.01,0.1,1],'YTick',[0:0.01:0.03],'FontSize',20,'Box','off');
set(gca,'XTickLabel',[0.01,0.1,1]);
xlabel('Centre Contrast'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
rad=patch_diam+1;
maxdiam=rad*2;
len=151;
L=zeros(maxdiam,len+50)+0.5;

I=image_contextual_surround(patch_diam,patch_diam/2,0,grating_wavel,grating_wavel,0,0,0,0.25,0); %no surround
[a,b]=size(I);
L(rad-floor(a/2):rad+floor(a/2),rad-floor(a/2):rad+floor(a/2))=I;


I=image_contextual_surround(patch_diam,0,patch_diam/2,grating_wavel,grating_wavel,0,0,0,0.25,max(contrasts)); %iso-oriented surround
[a,b]=size(I);
L(rad-floor(a/2):rad+floor(a/2),ceil(len/2)-floor(a/2):ceil(len/2)+floor(a/2))=I;

I=image_contextual_surround(patch_diam,0,patch_diam/2,grating_wavel,grating_wavel,90,0,0,0.25,max(contrasts)); %perpendicular surround		
[a,b]=size(I);
L(rad-floor(a/2):rad+floor(a/2),len-floor(a/2):len+floor(a/2))=I;

imagesc(L);
axis('equal','off')
hold on
plot(mean([maxdiam,ceil(len/2)-maxdiam]),rad,'ro','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','r');

plot(mean([ceil(len/2)+maxdiam,len-maxdiam]),rad,'bo','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');

plot(mean([len+maxdiam,len+ceil(len/2)-maxdiam]),rad,'gs','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');

axis([0,len+ceil(len/2),0,maxdiam])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
