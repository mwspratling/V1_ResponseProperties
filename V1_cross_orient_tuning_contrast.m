function V1_cross_orient_tuning_contrast
grating_wavel=26;

pxsize=65;
iterations=6;

grating_angles=[0:22.5:179];
diff_angle=90;
contrasts=[0.25,0.125];
node=3;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

cont=0;
for contrast=contrasts
  cont=cont+1;
  figure(cont),clf;
  for test=[1:3]
	ga=0;
	for grating_angle=grating_angles
	  ga=ga+1;
	  if test==1
		I=image_orientation_square(pxsize,grating_wavel,2,grating_angle);
		I=I*0.5+0.5;
	  elseif test==2
		I=image_orientation_square(pxsize,grating_wavel,2,grating_angle+diff_angle);
		I=I*contrast+0.5;
	  else
		I=image_cross_orientation_square(pxsize,grating_wavel,2,grating_angle,diff_angle,contrast*2);
		I=I*0.5+0.5;
	  end
	  [Ion,Ioff]=preprocess_image(I);

	  [a,b]=size(I);
	  %plot original image
	  maxsubplot(4,length(grating_angles),ga+(test-1)*length(grating_angles)),
	  imagesc(I,[0,1]);
	  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	  drawnow;
  
	  %perform competition
	  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	  y=mean(Y,4);
	  resp(test,ga)=y(ceil(a/2),ceil(b/2),node);
	end
  end
  clf;
  subplot(2,2,1);
  plot(grating_angles,resp(1,:),'k--','LineWidth',3,'MarkerSize',9,'MarkerFaceColor','w')
  hold on
  plot(grating_angles,resp(2,:),'b--','LineWidth',3,'MarkerSize',9,'MarkerFaceColor','w')
  plot(grating_angles,resp(3,:),'r-','LineWidth',3,'MarkerSize',9,'MarkerFaceColor','w')
  axis([0,180,0,0.01]);
  set(gca,'XTick',[0:45:180],'YTick',[0:0.01:0.05],'FontSize',20,'Box','off');
  xlabel('Orientation (degrees)'),ylabel('Response')

  maxsubplot(11,11,104)
  I=image_cross_orientation_square(pxsize,grating_wavel,2,90,diff_angle,contrast);
  I=I*0.5+0.5;
  imagesc(I,[0,1]);
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
  cmap=colormap('gray');
  drawnow;  
end
