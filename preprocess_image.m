function [Xon,Xoff]=preprocess_image(I)

LoG=-fspecial('log',9,1);
%To avoid using Image Processing toolbox try:
%LoG=gauss2D(0.9999,0,1,9)-gauss2D(1.0001,0,1,9);

%normalise weights
tmp=LoG;
tmp(find(tmp<0))=0;
LoG=LoG./sum(sum(tmp));

%calculare LGN neuron responses to input image
X=conv2(I,LoG,'same');

%apply gain to response
X=(2.*pi.*X);

%apply saturation to response
X=tanh(X);

%split into ON and OFF channels
Xon=X;
Xon(find(Xon<0))=0;
Xoff=-X;
Xoff(find(Xoff<0))=0;


function gauss=gauss2D(sigma,orient,aspect,pxsize)
%function gauss=gauss2D(sigma,orient,aspect)
%
% This function produces a numerical approximation to Gaussian function with
% variable aspect ratio.
% Parameters:
% sigma  = standard deviation of Gaussian envelope, this in-turn controls the
%          size of the result (pixels)
% orient = orientation of the Gaussian clockwise from the vertical (degrees)
% aspect = aspect ratio of Gaussian envelope (0 = no "width" to envelope, 
%          1 = circular symmetric envelope)
% pxsize = the size of the filter (optional). If not specified size is 5*sigma.
if nargin>3
  sz=pxsize;
else
  sz=fix(5*sigma);
end
if mod(sz,2)==0, sz=sz+1;end %mask has odd dimension
 
[x y]=meshgrid(-fix(sz/2):fix(sz/2),fix(-sz/2):fix(sz/2));
 
% Rotation 
orient=-orient*pi/180;
x_theta=x*cos(orient)+y*sin(orient);
y_theta=-x*sin(orient)+y*cos(orient);

gauss=exp(-.5*( ((x_theta.^2)./(sigma.^2)) ...
				+ ((y_theta.^2)./((aspect*sigma).^2)) ));
gauss=gauss./sum(sum(gauss));