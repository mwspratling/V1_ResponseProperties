function V1_cross_orient_orient_surr
crop=0;
grating_wavel=6;
patch_diam=19;

iterations=12;
contrast=0.5;

grating_angles=[-90:22.5:90];
angle=0;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
for test=2:3
  i=0;
  for ga=grating_angles
	i=i+1;
	fprintf(1,'.%i.',i); 
	if test==1 %no surround (std cross-orientation suppression)
	  I=image_cross_orientation_sine_circular_surr(patch_diam,0,20,...
						grating_wavel,grating_wavel,grating_wavel,...
						angle,ga,0,0,0,0,...
						contrast/2,contrast/2,0);
	elseif test==2 %iso-oriented surround
	  I=image_cross_orientation_sine_circular_surr(patch_diam,0,20,...
						grating_wavel,grating_wavel,grating_wavel,...
						angle,ga,0,0,0,0,...
						contrast/2,contrast/2,contrast);
	else %cross-oriented surround
	  I=image_cross_orientation_sine_circular_surr(patch_diam,0,20,...
						grating_wavel,grating_wavel,grating_wavel,...
						angle,ga,ga,0,0,0,...
						contrast/2,contrast/2,contrast);
	end
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	%plot original image
	maxsubplot(3,length(grating_angles),i),
	imagesc(I(crop+1:a-crop,crop+1:b-crop),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(grating_angles),i+length(grating_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(grating_angles),i+2*length(grating_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	sc(test,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

figure(1),clf
subplot(3,2,[4,6])
%plot(grating_angles,sc(1,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
plot(grating_angles,sc(2,:),'b-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
plot(grating_angles,sc(3,:),'g-d','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([-90,90,0,0.01])
set(gca,'XTick',[-90:45:90],'YTick',[0:0.002:0.02],'FontSize',20,'Box','off');
xlabel('Orientation (degrees)'),ylabel('Response')

figure(2),clf
maxsubplot(3,2,6)
diam=patch_diam;
patch_diam=patch_diam+30;
spacingh=patch_diam*2;
spacingv=patch_diam*1.25;
L=zeros(patch_diam+1*spacingv+1,2*spacingh+patch_diam+1)+0.5;
centv=ceil(patch_diam/2);
gal=length(grating_angles);
for test=2:3
  centh=ceil(patch_diam/2);
  for ga=grating_angles([2,ceil(gal/2),gal-1])
	if test==1 %no surround (std cross-orientation suppression)
	  I=image_cross_orientation_sine_circular_surr(diam,0,15,...
						grating_wavel,grating_wavel,grating_wavel,...
						angle,ga,0,0,0,0,...
						contrast/2,contrast/2,0);
	elseif test==2 %iso-oriented surround
	  I=image_cross_orientation_sine_circular_surr(diam,0,15,...
						grating_wavel,grating_wavel,grating_wavel,...
						angle,ga,0,0,0,0,...
						contrast/2,contrast/2,contrast);
	else %cross-oriented surround
	  I=image_cross_orientation_sine_circular_surr(diam,0,15,...
						grating_wavel,grating_wavel,grating_wavel,...
						angle,ga,ga,0,0,0,...
						contrast/2,contrast/2,contrast);
	end
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
	centh=centh+spacingh;
  end
  centv=centv+spacingv;
end
imagesc(L);
axis('equal','off')
ends=20;
axis([-ends,2*spacingh+patch_diam+ends,0,patch_diam+1*spacingv])
hold on
%plot(mean([spacingh,patch_diam]),ceil(patch_diam/2)+1,'ro','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(mean([spacingh,patch_diam]),ceil(patch_diam/2)+1+0*spacingv,'bs','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(mean([spacingh,patch_diam]),ceil(patch_diam/2)+1+1*spacingv,'gd','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');

%plot(mean([spacingh+patch_diam,2*spacingh]),ceil(patch_diam/2)+1,'ro','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(mean([spacingh+patch_diam,2*spacingh]),ceil(patch_diam/2)+1+0*spacingv,'bs','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(mean([spacingh+patch_diam,2*spacingh]),ceil(patch_diam/2)+1+1*spacingv,'gd','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');

cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
