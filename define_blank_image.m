function [I,size]=define_blank_image(size,len,gap);
size=size*(gap+len);
I=zeros(size)+0.5;
