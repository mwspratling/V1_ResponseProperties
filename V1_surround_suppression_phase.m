function V1_surround_suppression_phase
crop=0;
grating_wavel=6;
phases=[0:30:390];
node=1;
patch_diam=13;
iterations=30;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
i=0;
for phase=phases
  i=i+1;
  fprintf(1,'.%i.',i); 
   %contextual surround:
  I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,...
							  grating_wavel,0,0,phase,0.95,0.95);
  if phase>360	%centre patch alone
	I=image_contextual_surround(patch_diam,patch_diam,0,grating_wavel,...
								grating_wavel,0,0,phase,0.95,0);
  end
  [Ion,Ioff]=preprocess_image(I);
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(phases),i),
  imagesc(I(:,:,1),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(phases),i+length(phases)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %perform competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(phases),i+2*length(phases)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  sc(i)=y(ceil(a/2),ceil(b/2),node);
end
figure(1),clf
subplot(3,2,[4,6])
plot(phases,sc,'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0,360,0,0.06])
set(gca,'XTick',[0:90:360],'YTick',[0:0.02:0.06],'FontSize',20,'Box','off');
xlabel('Phase (degrees)'),ylabel('Response')
hold on
%plot activity to centre alone
plot([0,360],sc(length(sc)).*[1,1],'b-','LineWidth',4);

figure(2),clf
maxsubplot(5,2,8)
maxdiam=3*patch_diam;
spacingh=(275-maxdiam)/2;
L=zeros(maxdiam,2*spacingh+maxdiam)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+1;
for phase=[0,180,360]
  I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,grating_wavel,0,0,phase,0.95,0.95); %contextual surround
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=50-maxdiam/2;
axis([-ends,2*spacingh+maxdiam+ends,0,maxdiam])
hold on
centv=ceil(maxdiam/2)+1;
plot(mean([maxdiam,spacingh]),centv,'ro','LineWidth',4,'MarkerSize',8,'MarkerFaceColor','w');
plot(mean([maxdiam+spacingh,2*spacingh]),centv,'ro','LineWidth',4,'MarkerSize',8,'MarkerFaceColor','w');
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
