function V1_spatial_frequency_tuning
crop=0;
spatial_freq=1./[12,8,6,4,3];
grating_wavels=1./spatial_freq;

iterations=12;
patch_diam=15;
phase=0;
grating_angle=0;
contrast=0.25;
node=1;


[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
i=0;
clear Ion Ioff;
for gw=grating_wavels
  i=i+1;
  fprintf(1,'.%i.',i); 
  I=image_circular_grating(patch_diam,20,gw,grating_angle,phase,contrast); 
  [Ion,Ioff]=preprocess_image(I);
  [a,b]=size(I);
  
  %plot original image
  maxsubplot(3,length(grating_wavels),i),
  imagesc(I(:,:,1),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(grating_wavels),i+length(grating_wavels)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %perform competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(grating_wavels),i+2*length(grating_wavels)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  sc(i)=y(ceil(a/2),ceil(b/2),node);
end

figure(1),clf
subplot(3,2,[4,6])
semilogx(spatial_freq,sc,'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([0.05,0.5,0,0.012])
set(gca,'XTick',[0.05,0.1,0.2,0.5],'YTick',[0:0.005:0.012],'FontSize',20,'Box','off');
xlabel('Spatial Freq. (cycles/pixel)'),ylabel('Response')
figure(2),clf
maxsubplot(5,2,8)
maxdiam=patch_diam*3;
spacingh=(125-maxdiam)/2;
L=zeros(maxdiam+2,2*spacingh+maxdiam+2)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+1;
for gw=grating_wavels([1,3,5])
  I=image_circular_grating(patch_diam,patch_diam,gw,grating_angle,phase,contrast); %orientation tuning
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=20;
axis([-ends,2*spacingh+maxdiam,0.75*patch_diam,maxdiam+2-0.75*patch_diam])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

