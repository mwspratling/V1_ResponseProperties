function V1_surround_suppression_cent_cont_iso
crop=0;
iterations=20;
grating_wavel=6;

contrasts=[0,0.03,0.06,0.13,0.25,0.5];
patch_diam=15;

node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
j=0;
for conts=contrasts
  j=j+1;
  fprintf(1,'.%i.',j); 
  i=0;
  clear Ion Ioff;
  for contc=contrasts
	i=i+1;
	fprintf(1,'.%i.',i); 
	I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,grating_wavel,0,0,0,contc,conts);
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(Ion);
	
	%plot original image
	maxsubplot(3,length(contrasts),i),
	imagesc(I(crop+1:a-crop,crop+1:b-crop,:),[0,0.5]); 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),i+length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(contrasts),i+2*length(contrasts)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	sc(j,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

figure(1),clf
zeroOffset=0.03
subplot(3,2,[4,6])
plot(log(zeroOffset+contrasts),sc(1,:),'r-o','LineWidth',1,'MarkerSize',6,'MarkerFaceColor','r');
hold on
plot(log(zeroOffset+contrasts),sc(2,:),'r-o','LineWidth',2,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(3,:),'r-o','LineWidth',3,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(4,:),'r-o','LineWidth',4,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+contrasts),sc(5,:),'r-o','LineWidth',5,'MarkerSize',6,'MarkerFaceColor','r');
plot(log(zeroOffset+[0.0125,0.0125]),[0,0.5],'w','LineWidth',12);
axis([log(zeroOffset+0),log(zeroOffset+0.5),0,0.0275])
set(gca,'XTick',log(zeroOffset+[0,0.03,0.13,0.5]),'YTick',[0:0.01:0.03]);
set(gca,'XTickLabel',[0,0.03,0.13,0.5],'FontSize',20,'Box','off');
xlabel('Centre Contrast'),ylabel('Response')
figure(2),clf
maxsubplot(5,2,8)
maxdiam=patch_diam*2;
spacingh=(150-maxdiam)/2;
L=zeros(maxdiam+1,2*spacingh+maxdiam)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+2;
for contc=contrasts([1,4,6])
  I=image_contextual_surround(patch_diam,0,patch_diam/2,grating_wavel,grating_wavel,0,0,0,contc,max(contrasts));
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=10;
axis([-ends,2*spacingh+maxdiam+ends,0,maxdiam])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

