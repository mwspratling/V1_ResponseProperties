function V1_orientation_tuning_diam
crop=0;
grating_wavel=6;

grating_angles=[-45:15:45];

iterations=12;
patch_diams=[7,13,19];
phase=0;
contrast=0.5;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
j=0;
for diam=patch_diams
  j=j+1;
  i=0;
  clear Ion Ioff;
  for ga=grating_angles
	i=i+1;
	fprintf(1,'.%i.',i); 
	I=image_circular_grating(diam,20,grating_wavel,ga,phase,contrast);
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);

	%plot original image
	maxsubplot(3,length(grating_angles),i),
	imagesc(I(:,:,1),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(grating_angles),i+length(grating_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.1]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(grating_angles),i+2*length(grating_angles)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	sc(j,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

%for j=1:3, sc(j,:)=sc(j,:)./max(sc(j,:)); end

figure(1),clf
subplot(3,2,[4,6])
plot(grating_angles,sc(3,:),'b-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
plot(grating_angles,sc(2,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(grating_angles,sc(1,:),'g-d','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([-45,45,0,0.022])
set(gca,'XTick',[-45:45:45],'YTick',[0:0.01:0.03],'FontSize',20,'Box','off');
xlabel('Orientation (degrees)'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
patch_diam=13;
maxdiam=patch_diam;
spacingh=25;
L=zeros(maxdiam+2,2*spacingh+maxdiam+2)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+1;
gal=length(grating_angles);
for ga=grating_angles([1,ceil(gal/2),gal])
	I=image_circular_grating(patch_diam,0*patch_diam,grating_wavel,ga,phase,contrast);
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
	centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
axis([-0,80,0,maxdiam+2])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

