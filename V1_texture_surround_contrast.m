function V1_texture_surround_contrast
iterations=8;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;
node=3;
spacing=2;
barlen=3;
crop=2*(barlen+spacing);
%DEFINE TEST IMAGES
Im{1}=image_isolated(7,barlen,spacing,45);
Im{2}=image_uniform(7,barlen,spacing,45);
Im{3}=image_popout(7,barlen,spacing,-45);
Im{4}=image_uniform_contrast(7,barlen,spacing,45);
Im{5}=image_popout_contrast(7,barlen,spacing,-45);

%find location of centre of RF
xcoord=mean(find(max(Im{1})>0.75))
ycoord=mean(find(max(Im{1}')>0.75))

clf
for k=1:length(Im)
  fprintf(1,'.%i.',k); 
  I=Im{k};
  [Ion,Ioff]=preprocess_image(I);
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(Im),k),
  imagesc(I(crop+1:a-crop,crop+1:b-crop,:),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(Im),k+length(Im)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.025]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;

  %perform competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(Im),k+2*length(Im)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.025]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;

  respY(k,:)=Y(xcoord,ycoord,node,:);

  resp(k)=y(xcoord,ycoord,node);
end

cmap=colormap('gray');
%resp=resp./resp(1);

figure(1), clf
axes('Position',[0.145,0.55,0.46,0.4]),
bar([1:length(Im)],resp,'b')
top=max(max(resp));
axis([0.25,5.75,0,1.05*top])
set(gca,'XTickLabel',[],'FontSize',20,'Box','off');
ylabel('Response');
for i=1:length(Im)
  maxsubplot(7,12,38+i);
  imagesc(Im{i}(crop+1:a-crop,crop+1:b-crop),[0,1])
  axis('equal','tight')
  set(gca,'XTick',[],'YTick',[]);
end

figure(2),clf

t=[0:iterations];
respY=[zeros(k,1),respY];
plot(t,respY','LineWidth',4)
set(gca,'XTick',[0:5:10],'YTick',[0:0.01:0.03],'FontSize',12);
xlabel('Iteration'),ylabel('Response')
