function [xlocations,ylocations]=define_grid(size,len,gap,angle)
hlen=fix(len/2);
height=len;
width=len;
if abs(angle)<45
  height=ceil(abs(sin(angle*pi/180))*len);
  if mod(height,2)==0, height=height+1; end
elseif abs(angle)>45
  width=ceil(cos(angle*pi/180)*len);
  if mod(width,2)==0, width=width+1; end
end
%create regular texture
xlocations=1+hlen:gap+height:size-hlen;
ylocations=1+hlen:gap+width:size-hlen;
