function V1_temporal_frequency_tuning_diam
crop=0;
grating_wavel=6;
patch_diams=[7,13,19];

iterations=12;
drift_rates=[1,2,4,10,20,60,180];
contrast=0.5;
angle=0;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
j=0;
for diam=patch_diams
  j=j+1;
  i=0;
  clear Ion Ioff;
  for drift_rate=drift_rates
	i=i+1;
	fprintf(1,'.%i.',i); 
	phase=0;
	for t=1:iterations
	  %I=image_square_grating(diam,20,grating_wavel,angle,phase,contrast);
	  I=image_circular_grating(diam,20,grating_wavel,angle,phase,contrast);
	  [Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I);
	  phase=phase+drift_rate;
	end
	[a,b]=size(I);
	%plot original image
	maxsubplot(3,length(drift_rates),i),
	imagesc(I(:,:,1),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(drift_rates),i+length(drift_rates)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,1]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(drift_rates),i+2*length(drift_rates)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.025]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
  
	sc(j,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

%for j=1:3, sc(j,:)=sc(j,:)./max(sc(j,:)); end

figure(1),clf
drift_rates=drift_rates./360; %convert to cycles / iteration
zeroOffset=0.0
subplot(3,2,[4,6])
plot(log(zeroOffset+drift_rates),sc(3,:),'b-s','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
plot(log(zeroOffset+drift_rates),sc(2,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
plot(log(zeroOffset+drift_rates),sc(1,:),'g-d','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([log(zeroOffset+min(drift_rates)),log(zeroOffset+max(drift_rates)),0,0.022]);
set(gca,'XTick',log([0.005,0.05,0.5]),'YTick',[0:0.01:0.02]);
set(gca,'XTickLabel',[0.005,0.05,0.5],'FontSize',20,'Box','off');
xlabel('Drift Rate (cycles/iteration)'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
patch_diam=13;
maxdiam=patch_diam;
spacingh=25;
L=zeros(maxdiam+2,2*spacingh+maxdiam+2)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+1;
drl=length(drift_rates);
for drift_rate=drift_rates([1,ceil(drl/2),drl])
  phase=0+drift_rate;
  I=image_circular_grating(patch_diam,0,grating_wavel,angle,phase,contrast);
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
axis([-0,80,0,maxdiam+2])
cmap=colormap('gray');
