function V1_temporal_frequency_tuning
crop=14;
grating_wavel=6;
patch_diam=50;

iterations=12;
drift_rates=[1,2,4,10,20,60,180];
angle=90;
node=5;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
i=0;
for drift_rate=drift_rates
  i=i+1;
  fprintf(1,'.%i.',i); 
  phase=0;
  for t=1:iterations
	I=image_square_grating(patch_diam,0,grating_wavel,angle,phase,0.25);
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I);
	phase=phase+drift_rate;
  end
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(drift_rates),i),
  imagesc(Ion(:,:,1),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(drift_rates),i+length(drift_rates)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %perform competition 
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(drift_rates),i+2*length(drift_rates)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.025]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  sc(i)=y(ceil(a/2),ceil(b/2),node);
  %sc(i)=sum(sum(sum(y(crop+1:a-crop,crop+1:b-crop,:))));
end

figure(1),clf
drift_rates=drift_rates./360; %convert to cycles / iteration
subplot(3,2,[4,6])
semilogx(drift_rates,sc,'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
axis([min(drift_rates)./2,max(drift_rates).*2,0,0.006]);
set(gca,'XTick',[0.005,0.05,0.5],'YTick',[0:0.001:0.005],'FontSize',20,'Box','off');
%axis([min(drift_rates)./2,max(drift_rates).*2,0,10]);
%set(gca,'XTick',[0.005,0.05,0.5],'YTick',[0:2:10],'FontSize',20,'Box','off');

set(gca,'XTickLabel',[0.005,0.05,0.5]);
xlabel('Drift Rate (cycles/iteration)'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
spacingh=ceil((275-patch_diam)/2);
L=zeros(patch_diam,2*spacingh+patch_diam)+0.5;
centv=ceil(patch_diam/2)+1;
centh=ceil(patch_diam/2)+1;
for drift_rate=drift_rates
  phase=0+drift_rate;
  I=image_square_grating(patch_diam,0,grating_wavel,angle,phase,1);
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=50-patch_diam/2;
axis([-ends,2*spacingh+patch_diam+ends,-25,patch_diam])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
