function V1_cross_orient_spatial_freq
crop=0;
spatial_freq=1./[20,12,8,6,4,3,2,1.5];
mask_wavels=1./spatial_freq;
grating_wavel=6;
patch_diam=14;
image_size=51;

iterations=12;
phase=0;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
for test=1:2
  i=0;
  for mw=mask_wavels
	i=i+1;
	fprintf(1,'.%i.',i); 
	if test==1 %cross-orientation simulus
	  Ig=image_cross_orientation_sine(patch_diam,grating_wavel,mw,0,90, ...
									  phase,phase,0.1,0.25);
	else %spatial freq tuning stimulus
	  Ig=image_square_grating(patch_diam,0,mw,0,phase,0.1); 
	end
	%pad image so that they are the same size regardless of patch diameter
	I=zeros(image_size)+0.5;
	[a,b]=size(I);
	[ag,bg]=size(Ig);
	I(ceil(a/2)-floor(ag/2):ceil(a/2)+floor(ag/2),ceil(b/2)-floor(bg/2):ceil(b/2)+floor(bg/2))=Ig;
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	%plot original image
	maxsubplot(3,length(mask_wavels),i),
	imagesc(I(:,:),[0,1]);
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(mask_wavels),i+length(mask_wavels)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(mask_wavels),i+2*length(mask_wavels)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
		
	sc(test,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

figure(1),clf
subplot(3,2,[4,6])
semilogx(spatial_freq,sc(1,:),'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
semilogx([0.01,1],sc(2,find(mask_wavels==6)).*[1,1],'b-','LineWidth',2);
axis([0.035,1,0,0.0035])
set(gca,'XTick',[0.05,0.2,0.5],'YTick',[0:0.001:0.012],'FontSize',20,'Box','off');
xlabel('Spatial Freq. (cycles/pixel)'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
patchscale=5;
patch_diam=patch_diam*patchscale;
spacingh=patch_diam*2;
spacingv=patch_diam*1.25;
%L=zeros(patch_diam+spacingv+1,2*spacingh+patch_diam+1)+0.5;
L=zeros(patch_diam+1,2*spacingh+patch_diam+1)+0.5;
centv=ceil(patch_diam/2)+1;
for test=1:1
  centh=ceil(patch_diam/2)+1;
  for mw=mask_wavels([1,4,7])*patchscale
	phase=0;
	if test==1 %cross-orientation simulus
	  I=image_cross_orientation_sine(patch_diam,grating_wavel*patchscale,mw,0,90, ...
									 phase,phase,0.1,0.25);
	  
	else %spatial freq tuning
	  I=image_square_grating(patch_diam,0,mw,0,phase,0.5); 
	end
	[a,b]=size(I);
	L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
	centh=centh+spacingh;
  end
  centv=centv+spacingv;
end
imagesc(L);
axis('equal','off')
ends=60;
axis([-ends,2*spacingh+patch_diam+ends,0,patch_diam])
hold on

cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
