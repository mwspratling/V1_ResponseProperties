function I=image_orientation_square(sz,wavel,width,angle)
% sz  = the diameter of the image
% wavel  = the wavelength of the square wave (pixels)
% width = the width of the square wave
% angle = angle of grating

freq=2*pi./wavel;
angle=-angle*pi/180;

%define image size
if mod(sz,2)==0, sz=sz+1;end %image has odd dimension
I=zeros(sz);

%define mesh on which to draw sinusoids
[x y]=meshgrid(-fix(sz/2):fix(sz/2),fix(-sz/2):fix(sz/2));
yr=-x*sin(angle)+y*cos(angle);

%make sinusoids with values ranging from 0 to 1 (i.e. contrast is positive)
grating=0.5+0.5.*cos(freq*yr);

%use sinusoids to make square waves
thres=0.5+0.5.*cos(freq*(width./2));
grating(find(grating<thres))=0;
%grating(find(grating>=thres))=1;

%put togeter image from components
I=grating;
%I=I.*0.5;
