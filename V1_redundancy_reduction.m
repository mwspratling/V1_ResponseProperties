function V1_redundancy_reduction(I)
crop=0;
iterations=12;
node1=1;
node2=5;
shift1=2;
shift2=6;
[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

[Ion,Ioff]=preprocess_image(I);

figdown=2;
figacross=3;

clf
[a,b]=size(Ion);
	

%RESPONSE TO GABOR FILTERS
g1=wFFon(:,:,node1)-wFFoff(:,:,node1);
g2=wFFon(:,:,node2)-wFFoff(:,:,node2);

%record responses of filters at same orientation, different location
ynode1=conv2(I,g1,'valid');
ynode2=ynode1(shift1:size(ynode1,1),:);
ynode1=ynode1(1:size(ynode1,1)-shift1+1,:);
subplot(figdown,figacross,1)
plot_joint_histogram(ynode1(:),ynode2(:));

%record responses of filters at same orientation, different location
ynode1=conv2(I,g1,'valid');
ynode2=ynode1(shift2:size(ynode1,1),:);
ynode1=ynode1(1:size(ynode1,1)-shift2+1,:);
subplot(figdown,figacross,2)
plot_joint_histogram(ynode1(:),ynode2(:));

%record responses of filters at same location, different orientation
ynode1=conv2(I,g1,'valid');
ynode2=conv2(I,g2,'valid');
subplot(figdown,figacross,3)
plot_joint_histogram(ynode1(:),ynode2(:));


%RESPONSE OF DIM NETWORK

%initial dim response without competition
y=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);

%record responses of filters at same orientation, different location
ynode1=y(1:a-shift1+1,:,node1);
ynode2=y(shift1:a,:,node1);
subplot(figdown,figacross,1)
plot_joint_histogram(ynode1(:),ynode2(:));

%record responses of filters at same orientation, different location
ynode1=y(1:a-shift2+1,:,node1);
ynode2=y(shift2:a,:,node1);
subplot(figdown,figacross,2)
plot_joint_histogram(ynode1(:),ynode2(:));

%record responses of filters at same location, different orientation
ynode1=y(:,:,node1);
ynode2=y(:,:,node2);
subplot(figdown,figacross,3)
plot_joint_histogram(ynode1(:),ynode2(:));


%perform competition
y=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);

%record responses of filters at same orientation, different location
ynode1=y(1:a-shift1+1,:,node1);
ynode2=y(shift1:a,:,node1);
subplot(figdown,figacross,4)
H=plot_joint_histogram(ynode1(:),ynode2(:));
%subplot(figdown,figacross,4);
%H(15,:)=0;
%imagesc(H);

%record responses of filters at same orientation, different location
ynode1=y(1:a-shift2+1,:,node1);
ynode2=y(shift2:a,:,node1);
subplot(figdown,figacross,5)
H=plot_joint_histogram(ynode1(:),ynode2(:));
%subplot(figdown,figacross,8);
%H(15,:)=0;
%imagesc(H);

%record responses of filters at same location, different orientation
ynode1=y(:,:,node1);
ynode2=y(:,:,node2);
subplot(figdown,figacross,6)
H=plot_joint_histogram(ynode1(:),ynode2(:));
%subplot(figdown,figacross,12);
%H(15,:)=0;
%imagesc(H);


cmap=colormap('gray');
cmap=1-cmap;
colormap(cmap);

