function [wFFon,wFFoff]=dim_conv_V1_filter_definitions()
angles=[0:22.5:179];
wavel=6;
sigma=wavel./1.5;
aspect=1./sqrt(2);

%DEFINE feedforward WEIGHTS
k=0;
for phase=0:90:270
  for angle=angles
  k=k+1;

  %ON and OFF channels (modelled by Gabor split into positive and negative parts)
  gb=gabor(sigma,angle,wavel,phase,aspect);
  gb=single(gb);
  norm=sum(sum(abs(gb)));
  gb=gb./norm;
  wFFon(:,:,k)=max(0,gb);
  wFFoff(:,:,k)=max(0,-gb);
  end
end
