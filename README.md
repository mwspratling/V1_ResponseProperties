----------------------------------------------------------------------------
# INTRODUCTION
----------------------------------------------------------------------------

This code implements the simulation results reported in:

[M. W. Spratling (2010) Predictive coding as a model of response
properties in cortical area V1. Journal of Neuroscience 30(9):3531-3543.](https://nms.kcl.ac.uk/michael.spratling/Doc/v1_response_properties.pdf)

Please cite this paper if this code is used in, or to motivate, any publications. 

----------------------------------------------------------------------------
# USAGE
----------------------------------------------------------------------------

This code requires MATLAB and the Image Processing Toolbox. It was tested with
MATLAB Version 7.7 (R2008b) and Image Processing Toolbox Version 6.2 (R2008b).
However, it makes very little use of the Image Processing Toolbox and should be
easy to modify to work without this toolbox.

To use this software:
```
    run matlab 
    cd to the directory containing this code 
    run one of the functions with the name V1_*
```
Each of the V1_* functions runs one of the experiments reported in the above
publication. The following table gives details of which MATLAB function produces
each figure in this article.

Figure   |   MATLAB Function
---------|--------------------------------------------------------------
Main Article:|
5a       |   V1_orientation_tuning_contrast.m
5b       |   V1_size_tuning.m
5c       |   V1_size_tuning_contrast.m
5d       |   V1_spatial_frequency_tuning.m
5e       |   V1_spatial_frequency_tuning_contrast.m
5f       |   V1_temporal_frequency_tuning.m
         |
6a       |   V1_cross_orient_orient.m
6b & c   |   V1_cross_orient_contrast.m
6d       |   V1_cross_orient_spatial_freq.m
6e       |   V1_cross_orient_temporal_freq.m
         |
7        |   V1_cross_orient_tuning.m
8        |   V1_cross_orient_tuning_contrast.m
         |
9a-e     |   V1_surround_suppression_orient.m (edit parameters for each sub-figure)
10a      |   V1_surround_suppression_orient.m (edit parameters)
10b      |   V1_surround_suppression_cent_cont_iso.m
10c      |   V1_surround_suppression_cent_contrast.m
10d      |   V1_surround_suppression_surr_contrast.m
10e      |   V1_surround_suppression_plaid_contrast.m
10f      |   V1_surround_suppression_phase.m
         |
11f      |   V1_flankers_gilbert.m
11g & h  |   V1_texture_surround.m (edit parameters)
11i      |   V1_texture_surround_contrast.m
         |
12       |   V1_redundancy_reduction.m
         |
Supplemental Material:|
S1a      |   V1_orientation_tuning_diam.m
S1b      |   V1_spatial_frequency_tuning_diam.m
S1c      |   V1_temporal_frequency_tuning_diam.m
S1d      |   V1_cross_orient_orient_diam.m
S1e      |   V1_surround_suppression_orient_diam.m
         |
S2a & b  |   V1_cross_orient_orient_2masks.m
S2c-e    |   V1_cross_orient_orient_surr.m
S3       |   V1_cross_orient_tuning_sine.m
S4       |   V1_surround_suppression_orient.m (edit parameters)
S5a-c    |   V1_surround_suppression_cent_contrast.m (edit parameters)
S5d-f    |   V1_surround_suppression_surr_contrast.m (edit parameters)
         |
S6       |    V1_compare_error_and_prediction.m

