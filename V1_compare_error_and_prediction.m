function V1_compare_error_and_prediction
crop=0;
grating_wavel=6;
patch_diam=15;

iterations=12;
contrast=0.5;
phase=0;
maxy=0.031;

node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;
figure(1),clf
mainaxessize=[0.15,0.15,0.335,0.515];
mainaxes=axes('Position',mainaxessize); 
for test=1:5
  if test==1 %Circular Patch at Preferred Orientation
	I=image_circular_grating(patch_diam,patch_diam,grating_wavel,0,phase,contrast);
  elseif test==2 %Circular Patch at non-preferred Orientation
	I=image_circular_grating(patch_diam,patch_diam,grating_wavel,90,phase,contrast);
  elseif test==3 %cross-orientation simulus
	I=image_cross_orientation_sine(patch_diam*3,grating_wavel,grating_wavel,0,90,phase,phase,contrast,contrast);
	sz=fix(patch_diam*3/2);
	[x y]=meshgrid(-sz:sz,-sz:sz);
	radius=sqrt(x.^2+y.^2);
	I(find(radius>patch_diam*3/2))=0.5;
  elseif test==4 %contextual surround, iso-oriented
	I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,grating_wavel,0,phase,phase,contrast,contrast); 
  else
	I=image_contextual_surround(patch_diam,0,patch_diam,grating_wavel,grating_wavel,90,phase,phase,contrast,contrast); 
  end
  [a,b]=size(I);
  [Ion,Ioff]=preprocess_image(I);
	
  %perform competition
  [y,ron,roff,eon,eoff,Y,Eon]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  respY(test,:)=Y(ceil(a/2),ceil(b/2),node,:);
  respE(test,:)=Eon(ceil(a/2),ceil(b/2),:);

  p=y(ceil(a/2),ceil(b/2),node)
  patchsize=0.08;
  ypos=mainaxessize(2)+(mainaxessize(4)*p/maxy)-0.5*patchsize;
  axes('Position',[mainaxessize(1)+mainaxessize(3)-0.002,ypos,patchsize,patchsize]); 
  imagesc(I(:,:),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[]);
  cmap=colormap('gray');
  cmap=1-cmap;
  colormap(cmap);
  drawnow;
end

respY=[zeros(test,1),respY];
respE=[zeros(test,1),respE];
t=[0:iterations];

set(gcf,'CurrentAxes',mainaxes)
plot(t,respY','LineWidth',4)
axis([0,iterations,0,maxy])
set(gca,'XTick',[0:5:10],'YTick',[0:0.01:0.03],'FontSize',20,'Box','off');
xlabel('Iteration'),ylabel('Response')

figure(2),clf
axes('Position',mainaxessize); 
plot(t,respE','LineWidth',4)
axis([0,iterations,0,maxy])
set(gca,'XTick',[0:5:10],'YTick',[0:0.01:0.03],'FontSize',20,'Box','off');
xlabel('Iteration'),ylabel('Response')

maxy=max(max([respY,respE]))
