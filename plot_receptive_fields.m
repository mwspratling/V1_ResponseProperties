function plot_receptive_fields
figure(1),clf
[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

[a,b,z]=size(wFFon);
lim=max([max(max(max(wFFon))),max(max(max(wFFoff)))])

for j=1:z
  maxsubplot(4,8,j); imagesc(wFFon(:,:,j)-wFFoff(:,:,j),[-0.9*lim,0.9*lim]);
  set(gca,'XTick',[],'YTick',[]);
  axis('equal','tight')
end
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

figure(2),clf
maxsubplot(4,8,1), imagesc(wFFon(:,:,8),[-0.9*lim,0.9*lim]);
  set(gca,'XTick',[],'YTick',[],'FontSize',12);
  axis('equal','tight')
  title('ON')
maxsubplot(4,8,2), imagesc(wFFoff(:,:,8),[-0.9*lim,0.9*lim]);
  set(gca,'XTick',[],'YTick',[],'FontSize',12);
  axis('equal','tight')
  title('OFF')
maxsubplot(4,8,3), imagesc(0.9*lim,[-0.9*lim,0.9*lim]); 
  axis('off')
  colorbar('West');

  
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);
