function V1_cross_orient_temporal_freq
crop=0;
grating_wavel=6;
patch_diam=51;

iterations=12;
mask_drift_rates=[-1,1,2,4,10,20,60,180];
test_drift_rate=2;

mask_angle=90;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
i=0;
for mask_drift_rate=mask_drift_rates
  i=i+1;
  fprintf(1,'.%i.',i);
  mask_phase=0;
  test_phase=0;
  for t=1:iterations
	if mask_drift_rate<0;
	  %get response without mask
	  I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,0,mask_angle,test_phase,mask_phase,0.5,0);
	else
	  I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,0,mask_angle,test_phase,mask_phase,0.5,0.5);
	  mask_phase=mask_phase+mask_drift_rate;
	end
	[Ion(:,:,t),Ioff(:,:,t)]=preprocess_image(I);
	test_phase=test_phase+test_drift_rate;
  end
  [a,b]=size(I);
  %plot original image
  maxsubplot(3,length(mask_drift_rates),i),
  imagesc(I(:,:,1),[0,1]);
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %initial response without competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
  y=mean(Y,4);
  maxsubplot(3,length(mask_drift_rates),i+length(mask_drift_rates)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,1]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
  
  %perform competition
  [y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
  y=mean(Y,4);
  maxsubplot(3,length(mask_drift_rates),i+2*length(mask_drift_rates)),
  imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
  axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
  drawnow;
    
  sc(i)=y(ceil(a/2),ceil(b/2),node);
end

figure(1),clf
y_nomask=sc(1);
sc=sc(2:length(sc));
mask_drift_rates=mask_drift_rates(2:length(mask_drift_rates));
mask_drift_rates=mask_drift_rates./360; %convert to cycles / iteration
subplot(3,2,[4,6])
semilogx(mask_drift_rates,sc,'r-o','LineWidth',4,'MarkerSize',12,'MarkerFaceColor','w');
hold on
semilogx([min(mask_drift_rates)./2,max(mask_drift_rates).*2],y_nomask.*[1,1],'b-','LineWidth',2,'MarkerSize',12,'MarkerFaceColor','w');
axis([min(mask_drift_rates)./2,max(mask_drift_rates).*2,0,0.008]);
set(gca,'XTick',[0.005,0.05,0.5],'YTick',[0:0.002:0.008],'FontSize',20);
set(gca,'XTickLabel',[0.005,0.05,0.5],'Box','off');
xlabel('Drift Rate (cycles/iteration)'),ylabel('Response')

figure(2),clf
maxsubplot(5,2,8)
spacingh=ceil((275-patch_diam)/2);
L=zeros(patch_diam,2*spacingh+patch_diam)+0.5;
centv=ceil(patch_diam/2)+1;
centh=ceil(patch_diam/2)+1;
for mask_drift_rate=mask_drift_rates
  I=image_cross_orientation_sine(patch_diam,grating_wavel,grating_wavel,0,mask_angle,0,mask_phase,0.5,0.5);
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
  mask_phase=0+mask_drift_rate;
end
imagesc(L);
axis('equal','off')
ends=50-patch_diam/2;
axis([-ends,2*spacingh+patch_diam+ends,-25,patch_diam])

cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

