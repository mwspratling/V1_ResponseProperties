function I=draw_inverse_line(I,x,y,line,angle,contrast)
if nargin<6
  contrast=1;
end
line=imrotate(line,angle,'bilinear','crop');
line=line(2:size(line)-1,2:size(line)-1);
line(find(line<=0.5))=0;
line(find(line>0.5))=1;
line=line.*contrast;
length=size(line,1);
hlen=fix(length/2);
I(x-hlen:x+hlen,y-hlen:y+hlen)=min(I(x-hlen:x+hlen,y-hlen:y+hlen),1-line);
