function V1_size_tuning_contrast
crop=0;
grating_wavel=6;

diams=[3:4:31];
contrasts=[0.06,0.13,0.25,0.5,1];

iterations=8;
phase=0;
node=1;

[wFFon,wFFoff]=dim_conv_V1_filter_definitions;

clf
j=0;
for cont=contrasts
  j=j+1;
  fprintf(1,'.%i.',j); 
  i=0;
  for diam=diams
	i=i+1;
	%a grating:
	I=image_contextual_surround(diam,max(diams)-diam/2,0,grating_wavel,grating_wavel,0,phase,0,cont,0);
	[Ion,Ioff]=preprocess_image(I);
	[a,b]=size(I);
	
	%plot original image
	maxsubplot(3,length(diams),i),
	imagesc(I(crop+1:a-crop,crop+1:b-crop,:),[0,1]); 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%initial response without competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,1);
	y=mean(Y,4);
	maxsubplot(3,length(diams),i+length(diams)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.05]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;
	
	%perform competition
	[y,ron,roff,eon,eoff,Y]=dim_conv_on_and_off(wFFon,wFFoff,Ion,Ioff,iterations);
	y=mean(Y,4);
	maxsubplot(3,length(diams),i+2*length(diams)),
	imagesc(y(crop+1:a-crop,crop+1:b-crop,node),[0,0.01]), 
	axis('equal','tight'), set(gca,'XTick',[],'YTick',[],'FontSize',11);
	drawnow;

	sc(j,i)=y(ceil(a/2),ceil(b/2),node);
  end
end

figure(1),clf
subplot(3,2,[4,6])
plot(diams,sc(1,:),'r-o','LineWidth',1,'MarkerSize',6,'MarkerFaceColor','r');
hold on
plot(diams,sc(2,:),'r-o','LineWidth',2,'MarkerSize',6,'MarkerFaceColor','r');
plot(diams,sc(3,:),'r-o','LineWidth',3,'MarkerSize',6,'MarkerFaceColor','r');
plot(diams,sc(4,:),'r-o','LineWidth',4,'MarkerSize',6,'MarkerFaceColor','r');
plot(diams,sc(5,:),'r-o','LineWidth',5,'MarkerSize',6,'MarkerFaceColor','r');
axis([1,33,0,0.015])
set(gca,'XTick',[5:5:30],'YTick',[0:0.005:0.015],'FontSize',20,'Box','off');
xlabel('Diameter (pixels)'),ylabel('Response')
figure(2),clf
maxsubplot(5,2,8)
maxdiam=max(diams).*2;
spacingh=(175-maxdiam)/2;
L=zeros(maxdiam,2*spacingh+maxdiam)+0.5;
centv=ceil(maxdiam/2)+1;
centh=ceil(maxdiam/2)+2;
for diam=diams([1,5,7])
  I=image_contextual_surround(diam,max(diams)-diam/2,0,grating_wavel,grating_wavel,0,0,0,1,0);%a grating
  [a,b]=size(I);
  L(centv-floor(a/2):centv+floor(a/2),centh-floor(a/2):centh+floor(a/2))=I;
  centh=centh+spacingh;
end
imagesc(L);
axis('equal','off')
ends=33-maxdiam/2;
axis([-ends,2*spacingh+maxdiam+ends,maxdiam.*0.25,maxdiam.*0.75])
cmap=colormap('gray');
%cmap=1-cmap;
%colormap(cmap);

